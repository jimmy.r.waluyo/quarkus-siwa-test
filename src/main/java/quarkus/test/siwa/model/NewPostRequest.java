package quarkus.test.siwa.model;

import java.util.List;

import lombok.Data;

@Data
public class NewPostRequest {
    private String title;
    private String content;    
    private List<String> tags;
}
