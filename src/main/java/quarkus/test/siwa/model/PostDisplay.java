/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quarkus.test.siwa.model;

import java.util.List;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


/**
 *
 * @author Jemmy Rohmad Waluyo <jimmy.r.waluyo@gmail.com>
 */
@Data
public class PostDisplay {
    @Getter
    @Setter
    private Long id;
    
    @Getter
    @Setter
    private String title;
    
    @Getter
    @Setter
    private String content;    
    
    @Getter
    @Setter
    private List<String> tags;
}
