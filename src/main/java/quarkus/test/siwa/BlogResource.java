package quarkus.test.siwa;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quarkus.test.siwa.model.NewPostRequest;
import quarkus.test.siwa.model.Post;
import quarkus.test.siwa.model.PostDisplay;
import quarkus.test.siwa.model.Tag;

@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class BlogResource {
    public static final Logger LOG = LoggerFactory.getLogger(BlogResource.class);
    @Inject
    EntityManager entityManager;

    @POST
    @Path("/tag")
    @Transactional
    public void createTag(Tag tag) {
        LOG.info("creating a new tag: " + tag);
        tag.persist();
    }

    @GET
    @Path("/tags")
    @Transactional
    public List<Tag> listAllTags() {
        return Tag.listAll();
    }

    @POST
    @Path("/posts")
    @Transactional
    public PostDisplay createPost(NewPostRequest newPostRequest) {
        PostDisplay pd = new PostDisplay();
        
        LOG.info("new post request: " + newPostRequest);
        Post newPost = new Post(newPostRequest.getTitle(),newPostRequest.getContent());
        
        if (newPostRequest.getTags() != null && !newPostRequest.getTags().isEmpty())
            newPostRequest.getTags().forEach(tagLabel -> {
                LOG.info("\tloading tag from DB: " + tagLabel);
                // Tag loadedTag = Tag.find("name", tagName).firstResult();

                Session hibernateSession = entityManager.unwrap(Session.class);
                Tag loadedTagViaHbn = hibernateSession.bySimpleNaturalId(Tag.class).load(tagLabel);

                if (loadedTagViaHbn == null) {
                    Tag newTag = new Tag(tagLabel);

                    LOG.info("\tadding tag: " + newTag);

                    newTag.persist();
                     
                    newPost.addTag(newTag);
                } else {

                    LOG.info("\tadding tag: " + loadedTagViaHbn);

                    // newPost.addTag(loadedTag);
                    newPost.addTag(loadedTagViaHbn);
                    // LOG.info("persisting Post: " + newPost);
                    // newPost.persist();
                }
            });
        LOG.info("persisting Post: " + newPost);
        // newPost.persist();
        
        entityManager.persist(newPost);
        
        pd.setContent(newPost.getContent());
        pd.setTitle(newPost.getTitle());
            
        pd.setTags(newPost.getTags().stream().map(pt -> pt.getLabel()).collect(Collectors.toList()));
        pd.setId(newPost.id);
            
        return pd;

    }
    
    
    @POST
    @Path("/posts/{id}")
    @Transactional
    public PostDisplay updatePost(@PathParam("id") Long id,NewPostRequest newPostRequest) {
        
        PostDisplay pd = new PostDisplay();
        
        LOG.info("update request: " + newPostRequest);
        
        Session hibernateSession = entityManager.unwrap(Session.class);
        
        Post post = hibernateSession.byId(Post.class).load(id);
        
        if(post != null)
        {
          post.setContent(newPostRequest.getContent());
          post.setTitle(newPostRequest.getTitle());
          
          post.getTags().stream().forEach(tag -> post.removeTag(tag));
          
          post.getTags().clear();
          
          if (newPostRequest.getTags() != null && !newPostRequest.getTags().isEmpty())
            newPostRequest.getTags().forEach(tagLabel -> {
                LOG.info("\tloading tag from DB: " + tagLabel);                
                Tag loadedTagViaHbn = hibernateSession.bySimpleNaturalId(Tag.class).load(tagLabel);

                if (loadedTagViaHbn == null) {
                    Tag newTag = new Tag(tagLabel);

                    LOG.info("\tadding tag: " + newTag);

                    newTag.persist();
                     
                    post.addTag(newTag);
                } else {

                    LOG.info("\tadding tag: " + loadedTagViaHbn);

                    // newPost.addTag(loadedTag);
                    post.addTag(loadedTagViaHbn);
                    // LOG.info("persisting Post: " + newPost);
                    // newPost.persist();
                }
            });
          
            LOG.info("persisting Post: " + post);
            // newPost.persist();
            entityManager.persist(post);
            
            pd.setContent(post.getContent());
            pd.setTitle(post.getTitle());
            
            pd.setTags(post.getTags().stream().map(pt -> pt.getLabel()).collect(Collectors.toList()));
            pd.setId(post.id);
        }
        
        return pd;
    }

    @GET
    @Path("/listPost")
    @Transactional
    public List<PostDisplay> listAllPosts() {

        /*
        List<Post> posts = entityManager.createQuery(
            "select p " +
            "from Post p ", Post.class)
            .getResultList();
        return posts;
        */
        
        
        List<Post> posts = entityManager.createQuery(
            "select p " +
            "from Post p " +
            "join fetch p.tags pt " +
            "join fetch pt.tag ", Post.class)
            .getResultList();
        
        posts = posts.stream().distinct().collect(Collectors.toList());
        
        List<PostDisplay> list = new ArrayList<>();
        
        posts.stream().forEach(x -> {
             PostDisplay p = new PostDisplay();
            p.setContent(x.getContent());
            p.setTitle(x.getTitle());
            
            p.setTags(x.getTags().stream().map(pt -> pt.getLabel()).collect(Collectors.toList()));
            p.setId(x.id);
            
            list.add(p);
         });
        
        
        return list;
    }
}
